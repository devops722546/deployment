# CI/CD Pipeline for Automated Code Deployment

This CI/CD pipeline automates the process of deploying code from a GitHub or GitLab repository to a web server. Follow the steps below for installation, usage, and contributing to enhance the pipeline.

## Note

This pipeline should be deployed on GitLab only.

## Installation for Linux

Clone this project to your Local Machine.

```bash
git clone https://gitlab.com/devops722546/deployment
```

Navigate to the project directory.
```bash
cd deployment
```

Update the configuration file with the appropriate information.

```bash
nano .gitlab-ci.yml

```

Modify the following fields:

SERVER_IP: Replace with the IP address of your web server.

USERNAME: Replace with the username for accessing the web server.

PASSWORD: Replace with the password for accessing the web server.

Save and exit the editor.


Update the GitHub repository URL in the setup script

```bash
nano deploy_script.sh
```
Replace the GitHub URL with your GitHub repository URL with the latest code.

Save and exit the editor.

## Usage

After pushing the code to your GitLab repository, the pipeline will run automatically. Alternatively, you can trigger the pipeline manually.

Automatic Trigger:

The pipeline will automatically run when changes are pushed to the GitHub repository.

Manual Trigger:

Navigate to the CI/CD section of your GitLab project.

Find the pipeline corresponding to your branch.

Trigger the pipeline manually.

The pipeline will handle the deployment process, ensuring the latest code is deployed to the specified web server.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

NIL
